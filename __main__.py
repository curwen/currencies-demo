from flask import Flask, render_template, request
from gevent import monkey

from core.settings import CURRENCY_API_CLS, LOAD_DAYS
from core.helpers import get_module_attr, get_logger

app = Flask(__name__)

_logger = get_logger(__name__)

monkey.patch_all()


class CurrenciesServer:
    default_api_cls = CURRENCY_API_CLS

    def __init__(self, api_cls=None):
        self.logger = _logger
        self.currency_api = self._init_currency_api(api_cls)
        self._init_flask()

    def _init_currency_api(self, api_cls=None):
        if not api_cls:
            api_cls = get_module_attr(self.default_api_cls)
            if not api_cls:
                raise ImportError('Cannot initialize currency API handler: class not found', self.default_api_cls)
        currency_api = api_cls(logger=self.logger)
        self.logger.debug('Loaded currency API handler %s', api_cls.__name__)
        return currency_api

    def _init_flask(self):
        self.flask_app = app
        self.flask_app.add_url_rule('/', 'index', self.main_view, methods=['GET', 'POST'])
        self.flask_app.run(port=8888)

    def main_view(self):
        all_currencies = self.currency_api.get_available_currencies()
        if request.method == 'POST':
            currency = request.form['currency']
            error = None
            rates = self.currency_api.get_last_days_rates(
                currency,
                days=LOAD_DAYS,
                # to_currencies=['RUB', 'USD']
            )
            if rates is None:
                error = 'Cannot get data for selected currency {}'.format(currency)
                return render_template('rates.html', rates=None, error=error)
            return render_template('rates.html', rates=rates, error=error, currencies=all_currencies)
        return render_template('index.html', currencies=all_currencies)

if __name__ == '__main__':
    server = CurrenciesServer()
