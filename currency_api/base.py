from abc import ABC, abstractmethod
from typing import Sequence

from core.helpers import get_logger

_logger = get_logger(__name__)


class BaseCurrencyAPI(ABC):
    logger = None

    def __init__(self, logger=None):
        self.logger = logger or _logger

    @abstractmethod
    def get_current_rates(self, currency: str, to_currencies: Sequence = None) -> dict:
        """
        get actual rates for currency
        :param currency: currency code
        :param to_currencies: list of currency codes to get rates for (optional)
        :return: today rates dict
        """

    @abstractmethod
    def get_available_currencies(self) -> Sequence:
        """:return: list of all available currencies"""

    @abstractmethod
    def get_rates_for_ts(self, currency: str, timestamp: int, to_currencies: Sequence=None) -> dict:
        """
        get currency exchange rates for last N days
        :param currency: currency code
        :param timestamp: date to get historical rates for
        :param to_currencies: list of currency codes to get rates for (optional)
        :return: rates dict for passed date
        """

    @abstractmethod
    def get_historical_rates(self, currency: str, start_date, end_date=None,
                             to_currencies: Sequence = None) -> dict:
        """
        get currency exchange rates for last N days
        :param currency: currency code
        :param start_date: time range start
        :param end_date: time range end (defaults to current date)
        :param to_currencies: list of currency codes to get rates for (optional)
        :return: rates dict for passed dates interval
        """
