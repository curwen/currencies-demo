from typing import Sequence
from datetime import datetime, timedelta
from collections import defaultdict, OrderedDict


import gevent
import gevent.pool
import requests

from core import settings
from currency_api.base import BaseCurrencyAPI


class FixerCurrencyAPI(BaseCurrencyAPI):
    """fixer.io API helper class"""
    base_url = None
    api_key = None
    rates = None
    base_currency = 'EUR'
    _all_currencies = None

    def __init__(self, base_url: str = None, api_key=None, **kwargs):
        super().__init__(**kwargs)
        self.base_url = base_url or settings.FIXER_API_BASE_URL
        self.api_key = api_key or settings.FIXER_API_KEY
        # assuming that historical data cannot change, we can store'em locally to avoid unnecessary requests
        self.rates = defaultdict(dict)
        # preload data
        self.get_available_currencies()
        if settings.PRELOAD_DATA:
            self.get_last_days_rates(
                self.base_currency,
                days=settings.LOAD_DAYS
            )

    def get_available_currencies(self) -> Sequence:
        if not self._all_currencies:
            self._all_currencies = self._get_available_currencies()
        return self._all_currencies

    def get_current_rates(self, currency: str, to_currencies: Sequence = None) -> dict:
        now_ts = datetime.now().timestamp()
        return self.get_rates_for_ts(currency, now_ts, to_currencies)

    def get_last_days_rates(self, currency: str, days: int, to_currencies: Sequence = None):
        return self.get_historical_rates(
            currency,
            start_date=datetime.now()-timedelta(days=days),
            to_currencies=to_currencies
        )

    def get_historical_rates(self, currency: str, start_date: datetime, end_date: datetime = None,
                             to_currencies: Sequence = None) -> dict:
        end_date = end_date or datetime.now()
        days_range = self._get_days_in_range(start_date, end_date)

        pool = gevent.pool.Pool(settings.GEVENT_POOL_SIZE)
        greenlets = {}
        for day in days_range:
            day_gl = gevent.spawn(self.get_rates_for_day, currency, day, to_currencies)
            greenlets[day] = day_gl
            pool.add(day_gl)
        pool.join(timeout=settings.GEVENT_TIMEOUT)
        self.logger.debug('received currency rates for %i days', len(days_range))
        rates = OrderedDict([(day, greenlets[day].get()) for day in days_range])
        return rates

    def get_rates_for_ts(self, currency: str, timestamp: int, to_currencies: Sequence = None) -> dict:
        day = self._ts_to_date_str(timestamp)
        return self.get_rates_for_day(self.base_currency, day)

    def get_rates_for_day(self, currency: str, day: str, to_currencies: Sequence = None) -> dict:
        if not self.rates[day]:
            # can get rates only for base currency on FREE plan
            base_rates = self._get_rates(self.base_currency, day)
            self.rates[day] = self._calc_rates(base_rates)
        rates = self.rates[day][currency]
        if to_currencies:
            return {k: v for k, v in rates if k in to_currencies}
        return rates

    def _get_available_currencies(self) -> OrderedDict:
        query_url = self.base_url + 'symbols?access_key={api_key}'.format(api_key=self.api_key)
        try:
            response = requests.get(query_url)
            if response.status_code != 200:
                self.logger.error('[code: %s] %s', response.status_code, response.content)
                return None
            if not response.json()['success']:
                self.logger.error('[code: %s] %s', response.status_code, response.content)
                return None
            currencies = OrderedDict(sorted(response.json()["symbols"].items()))
            self.logger.debug('received currencies from API')
            return currencies
        except (requests.ConnectionError, AttributeError):
            self.logger.exception('Cannot retrieve data with query %s', query_url)
            return None

    def _get_rates(self, currency: str, day: str, to_currencies: Sequence = None) -> dict:
        endpoint = self.base_url + '{day}?access_key={api_key}&base={currency}'
        query_url = endpoint.format(
            day=day,
            api_key=self.api_key,
            currency=currency
        )
        if to_currencies:
            query_url += '&symbols=' + ','.join(to_currencies)
        try:
            response = requests.get(query_url)
            if response.status_code != 200:
                self.logger.error('[code: %s] %s', response.status_code, response.content)
                return None
            if not response.json()['success']:
                self.logger.error('[code: %s] %s', response.status_code, response.content)
                return None
            rates = OrderedDict(sorted(response.json()["rates"].items()))
            self.logger.debug('received currency rates from API: day %s', day)
            return rates
        except (requests.ConnectionError, AttributeError):
            self.logger.exception('Cannot retrieve data with query %s', query_url)
            return None

    def _calc_rates(self, base_rates: dict) -> dict:
        rates = {
            currency: OrderedDict(sorted(
                [(sec_cur, base_rates[sec_cur] / base_rates[currency])
                 for sec_cur in self._all_currencies.keys()]
            ))
            for currency in self._all_currencies.keys()
        }

        rates[self.base_currency] = base_rates
        return rates

    @staticmethod
    def _ts_to_date_str(timestamp: int):
        dt = datetime.fromtimestamp(timestamp)
        return dt.strftime(settings.FIXER_API_DATE_FORMAT)

    @staticmethod
    def _get_days_in_range(start_date: datetime, end_date: datetime) -> list:
        return [
            (start_date + timedelta(days=days)).strftime(settings.FIXER_API_DATE_FORMAT)
            for days in range((end_date - start_date).days + 1)
        ]
