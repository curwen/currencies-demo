import importlib
import logging
from itertools import islice

from core.settings import LOG_LEVEL


def get_logger(name: str=None, log_level: str=None):
    name = name or __name__
    log_level = log_level or LOG_LEVEL
    logger = logging.getLogger(name)
    logger.setLevel(log_level)
    # fh = logging.FileHandler('debug.log')
    # fh.setLevel(log_level)
    # logger.addHandler(fh)
    ch = logging.StreamHandler()
    ch.setLevel(log_level)
    logger.addHandler(ch)
    return logger


def get_module_attr(attr_path):
    try:
        module, attr = attr_path.rsplit('.', maxsplit=1)
    except ValueError:
        return None

    module = importlib.import_module(module)
    return getattr(module, attr, None)


def chunk_splitter_gen(iterable, chunk_size):

    for offset in range(0, len(iterable), chunk_size):
        yield list(islice(iterable, offset, offset + chunk_size))