# common settings

LOG_LEVEL = 'DEBUG'
CURRENCY_API_CLS = 'currency_api.fixer_api.FixerCurrencyAPI'
LOAD_DAYS = 30
PRELOAD_DATA = True

GEVENT_POOL_SIZE = 6
GEVENT_TIMEOUT = 60

# Fixer API constraints

FIXER_API_BASE_URL = 'http://data.fixer.io/api/'
FIXER_API_DATE_FORMAT = '%Y-%m-%d'
FIXER_API_KEY = '7255e24c01c239135d9695ba2a002736'
